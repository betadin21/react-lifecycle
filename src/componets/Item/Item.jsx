import React, { Component } from "react";
import styles from "./Item.module.css"

export default class Item extends Component{
    mouseEnterHandler = (e) =>{
        e.target.style.transform = "scale(1.7)"
    }
    mouseLeaveHandler = (e) =>{
        e.target.style.transform = "scale(1)"
    }

    render(){
        const {item} = this.props;
        return(
            <div className={styles.item}>
            <div className={styles.category}>{item.category} <span className={styles.id}>id#{item.id}</span></div>
            <div className={styles.item__img}>
                <img src={item.image} onMouseLeave={this.mouseLeaveHandler} onMouseEnter={this.mouseEnterHandler} alt="item-img" />
            </div>
            <div className={styles.title}>{item.title}</div>
            <div className={styles.description}>{item.description}</div>
            <div className={styles.count}>В наявності <span>{item.rating.count}</span></div>
            <div className={styles.rat}>
            <div className={styles.rating} data-total-value={item.rating.rate.toFixed()}>
                <div className={styles.rating__item} data-item-value="5">★</div>
                <div className={styles.rating__item} data-item-value="4">★</div>
                <div className={styles.rating__item} data-item-value="3">★</div>
                <div className={styles.rating__item} data-item-value="2">★</div>
                <div className={styles.rating__item} data-item-value="1">★</div>
             </div> 
            </div>
            <div className={styles.price}>{item.price}$</div>
            <div className={styles.buy}>Add to cart</div>
            
        </div>
        )
    }
}