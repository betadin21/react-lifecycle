import React, { Component } from "react";
import styles from "./Products.module.css";
import Item from "../Item/Item"

export default class Products extends Component {
  state = {
    items : []
  };
    componentDidMount(){
       let xhr = new XMLHttpRequest();
       xhr.open("get", "https://fakestoreapi.com/products");
       
       xhr.onload = () => {
        if(xhr.readyState === 4 && xhr.status === 200) {
            const response = JSON.parse(xhr.response);
            this.setState({
                items : response
            })
        }
       }
       xhr.onerror = () => {
        console.error("Помилка запиту на сервер");
       }
       xhr.send();

    }
  render() {
    const {items} = this.state;
    return (
      <>
        <h1 className={styles.text}>Каталог товарів</h1>
        <div className={styles.items__container}>
            {items.map(item=>{
             return <Item key={item.id} item ={item}/>
            })}
        </div>
        
      </>
    );
  }
}
